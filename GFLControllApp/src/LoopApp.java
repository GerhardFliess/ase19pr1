
public class LoopApp {

	public static void main(String[] args) {
		int[] werte = new int[] { 2, 3, 5, 8, 1, 2 };

		int max = werte[0];
		for (int index = 0; index < werte.length; index++) {
			if (werte[index] > max)
				max = werte[index];
		}

		max = werte[0];

		for (int temp : werte) {
			if (temp > max)
				max = temp;
		}
		
		double[] dwerte = new double[] { 2, 3, 5, 8, 1, 2 };
		
		double sum=0;
		for (double temp : dwerte) {
			sum = sum +temp;
		}			
		System.out.println(sum);

	}

}
