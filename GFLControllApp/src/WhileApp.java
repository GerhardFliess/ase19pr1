
public class WhileApp {

	public static void main(String[] args) {

		double konto = 2000;
		int jahre = 0;
		while (konto < 3000) {
			konto = konto + konto * 0.015;
			jahre++;
		}

		System.out.printf("nach %d Jahren haben sie %.2f� am konto ", jahre, konto);

	}

}
