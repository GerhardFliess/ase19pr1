
public class ConvertApp {
	public static void main(String[] args) {

		double dValue =10.5;
		int intValue = (int)dValue; // ja, ja, ich wei� es soll int sein...
		
		System.out.println(intValue);
		
		
		String wert = "100";
		int a = Integer.valueOf(wert);
		System.out.println(a); // als dezimalzahl
		System.out.println(Integer.valueOf(wert, 2)); // als bin�r zahl

		a = Integer.valueOf("-10");
		a = Integer.valueOf("abcdef", 16);

//		a = Integer.valueOf("Mimi");
//		System.out.println(a);

		String matrNr = "08154711";
		System.out.println(matrNr.charAt(1));

		// die 2.stelle aus der Zeichenkette in einen int verwandeln
		a = Integer.valueOf("" + matrNr.charAt(1));

		System.out.println(matrNr.length());
		System.out.println(a);
		
		double dWert = Double.valueOf("20.5");
		System.out.println(dWert);

	}
}
