
public class ArrayApp {
	public static void main(String[] args) {

		int[] feld = new int[] { 2, 3, 5, 8, 1 };
		System.out.println(feld);
		System.out.println(feld.length);
		
		int feld1[] = new int[10];
		feld1[0] = 2;
		// ....

		boolean boolArray[] = new boolean[3];
		System.out.println("bye");
	}
}
