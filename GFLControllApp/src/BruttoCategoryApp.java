
public class BruttoCategoryApp {

	public static void main(String[] args) {
		double price = 200;
		int taxCategory = 4;
		// 1 -> 20%
		// 2 -> 12%
		// 3->10%
		double result = 0; // ergebnis
		double tax = 0;

		// mit if die category auswerten und berechnen
		if (taxCategory == 1) {
			tax = 20;
		} else if (taxCategory == 2) {
			tax = 12;
		} else if (taxCategory == 3) {
			tax = 10;
		}

		result = price * (1 + tax / 100);
		System.out.println(result);

	}

}
