
public class PrintArrayApp {
	public static void main(String[] args) {

		int[] feld = new int[] { 2, 3, 5, 8, 1 };

		for (int index = 0; index < feld.length; index++) {
			int value = feld[index];
			System.out.printf("%d ", value);
		}
		System.out.println();
		
		int maxresult = findMax(feld);
		
		System.out.println(maxresult);
	}

	public static int findMax(int[] zahlen) {
		int max = zahlen[0];
		for (int index = 0; index < zahlen.length; index++) {
			if (zahlen[index] > max) {
				max = zahlen[index];
			}
		}
		return max;
	}
}
