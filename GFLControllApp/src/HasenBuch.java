
public class HasenBuch {

	public static void main(String[] args) {

		lesen(3);
		System.out.println();
		
		System.out.println(add(3));
	}

	public static void lesen(int i) {

		if (i == 0)
			return;

		System.out.println("ein hase der gern..");
		System.out.println("im buch stand:");
		lesen(i - 1);
		System.out.println(i);
	}

	public static int add(int i) {
		if (i == 0)
			return 0;
		
		return i + add(i - 1);
	}

}
