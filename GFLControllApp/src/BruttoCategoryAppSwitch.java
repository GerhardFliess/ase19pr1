
public class BruttoCategoryAppSwitch {

	public static void main(String[] args) {
		double price = 200;
		int taxCategory = 2;
		// 1 -> 20%
		// 2 -> 12%
		// 3->10%
		double result = 0; // ergebnis
		double tax = 0;

		switch (taxCategory) {
		case 1:
			tax = 20;
			break;
		case 2:
			tax = 12;
			break;
		case 3:
			tax = 10;
			break;
		default:
			System.out.println("keine unterszütze kategorie");
		}

		result = price * (1 + tax / 100);
		System.out.println(result);

	}

}
