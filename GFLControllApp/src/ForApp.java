
public class ForApp {

	public static void main(String[] args) {

		for (int index = 0; index < 3; index++) {
			System.out.print(index + " ");
		}

		System.out.println(); // nur eine leere zeile
		for (int index = 0; index < 6; index = index + 2) {
			System.out.print(index + " ");
		}

		System.out.println(); // nur eine leere zeile
		for (int index = 1; index < 8; index = index + 2) {
			System.out.print(index + " ");
		}

		System.out.println(); // nur eine leere zeile
		for (int index = 8; index >= 0; index = index - 2) {
			System.out.print(index + " ");
		}

		System.out.println(); // nur eine leere zeile
		for (int index = 7; index <= 10; index++) {
			System.out.print(index + " ");
		}

		System.out.println();
		int result = 0;
		for (int count = 0; count < 6; count++)
		{
			result = result + count;
	    	System.out.print(result + " ");
		}
		
		System.out.println();
		result = 0;
		for(int index =1;index<=10;index++)
		{
			result = result+index;
		}
		System.out.println(result);
		
		
		double fraction = 0;
		for(int index =1;index<=10;index++)
		{
			fraction =fraction+index/2.0;
		}
		
		System.out.println(fraction);
		
		fraction = 0;
		for(int index =2;index<=10;index++)
		{
			fraction =fraction+1.0/index;
		}
		
		System.out.println(fraction); // ln--> teilen umbrunch
		System.out.printf("das Ergebnis ist: %.5f %04d\n",fraction,result); 
		// \n --> zeilenumbruch
        // %.5f 5 nachkommastellen bei einer flie�kommazahl
		// %04d 4 stellige ganzzahl mit f�hrenden nullen
		// https://docs.oracle.com/javase/10/docs/api/java/util/Formatter.html
		
		System.out.println();
		System.out.println("fibonacci reihe");
		int f1 = 1;
		int f2 = 0;
		for (int count = 0; count < 7; count++) {
			result = f1 + f2;
			f1 = f2;
			f2 = result;
			System.out.print(f2 + " ");
		}
	}

}
