
public class ReturnApp {

	public static void main(String[] args) {

		int[] feld = new int[]{1, 3, 5, 2, 7, 6};
		System.out.println(contains(feld, 5));
		System.out.println(contains(feld, 8));

		containsConsole(feld, -1);
		containsConsole(feld, 3);
		containsConsole(feld, 8);

		System.out.println(position(feld, -2)); // --> -2
		System.out.println(position(feld, 2)); // --> 3
		System.out.println(position(feld, 10)); // -1

	}

	public static int position(int[] werteMenge, int gesuchterWert) {

		// is es positiv?
		if (gesuchterWert < 0) {
			return -2;
		}

		// nach der position suchen
		for (int position = 0; position < werteMenge.length; position++) {
			if (werteMenge[position] == gesuchterWert) {
				return position;
			}
		}

		return -1;

	}

	public static boolean contains(int[] werte, int such) {

		if (such < 0)
			return false;

		for (int wert : werte) {
			if (wert == such)
				return true;
		}

		return false;
	}

	public static void containsConsole(int[] werte, int such) {

		if (such < 0) {
			System.out.println("suchwert darf nicht negativ sein");
			return;
		}

		for (int wert : werte) {
			if (wert == such) {
				System.out.println("gefunden");
				return;
			}
		}

		System.out.println("nicht gefunden");
	}

}
