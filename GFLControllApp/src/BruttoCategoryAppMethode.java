
public class BruttoCategoryAppMethode {

	public static void main(String[] args) {
		double price = 200;
		int taxCategory = 4;
		calcPrice(taxCategory, price);
		calcPrice(2, 400);
	}

 	//                  name      parameter 
	private static void calcPrice(int category, double price) {
		// ergebnis
		// mit if die category auswerten und berechnen
		double tax = 0;
		double result = 0; 
		
		if (category == 1) {
			tax = 20;
		} else if (category == 2) {
			tax = 12;
		} else if (category == 3) {
			tax = 10;
		} else {
			System.out.println("keine unterszütze kategorie");
		}
		result = price * (1 + tax / 100);
		System.out.println(result);

	}
}
