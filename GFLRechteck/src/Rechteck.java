
public class Rechteck {

	double breite;
	double h�he;

	public Rechteck(double initialeBreite, double initialeH�he) {

		breite = initialeBreite;
		h�he = initialeH�he;
	}

	public double berechneFl�che() {
		return breite * h�he;
	}

	public void skaliere(double faktor) {
		breite = breite * faktor;
		h�he = h�he * faktor;
	}

	// macht eine String der das Objekt beschreibt
	public String toString() {

		return String.format("( %.2f , %.2f )", breite, h�he);
	}

}
