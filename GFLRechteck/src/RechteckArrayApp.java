
public class RechteckArrayApp {

	public static void main(String[] args) {

		int intArray[] = new int[]{1, 2, 3};
		intArray[1] = 4;

		for (int wert : intArray) {
			System.out.println(wert);
		}

		Rechteck r1 = new Rechteck(2, 3);

		Rechteck[] rechtecke = new Rechteck[3];
		rechtecke[0] = new Rechteck(1, 2);
		rechtecke[1] = r1;

		Rechteck[] rechtecke1 = new Rechteck[]{new Rechteck(4, 5),
				new Rechteck(5, 6), r1};
		print(rechtecke1);

		double sum = berechneSumme(rechtecke1);
		System.out.println(sum);
		
		for (Rechteck aktuellesRechteck : rechtecke1) {
			aktuellesRechteck.skaliere(2);
		}
		
		print(rechtecke1);
	}

	private static void print(Rechteck[] rechtecke1) {
		for (Rechteck aktuellesRechteck : rechtecke1) {
			System.out.println(aktuellesRechteck);
		}
	}

	private static double berechneSumme(Rechteck[] rechtecke1) {
		double sum = 0;
		for (Rechteck aktuellesRechteck : rechtecke1) {
			sum = sum + aktuellesRechteck.berechneFläche();
		}
		return sum;
	}

}
