
public class RechteckApp {

	public static void main(String[] args) {

		Rechteck a1 = new Rechteck(2, 4);
		Rechteck a2 = new Rechteck(5, 6);
		Rechteck a3 = new Rechteck(10, 100);

		System.out.println(a1.berechneFläche());
		System.out.println(a2.berechneFläche());
		System.out.println(a3.berechneFläche());

		a3.skaliere(0.5);
		System.out.println(a3.berechneFläche());
		System.out.println(a3);

		System.out.println();
		Rechteck a4 = a1;
		System.out.println(a4);
		System.out.println(a1);

		a4.skaliere(2);
		System.out.println(a4);
		System.out.println(a1);

		int a = 2;
		int b = a;
		a = a + 1;

		System.out.println(a);
		System.out.println(b);

	}

}
