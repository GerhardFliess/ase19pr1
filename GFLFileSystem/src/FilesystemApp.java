import at.campus02.filesystem.Datei;
import at.campus02.filesystem.Ordner;

public class FilesystemApp {
	public static void main(String[] args) {

		Datei test1 = new Datei("huhu.png", 500);
		System.out.println(test1);

		Datei test2 = new Datei("mimi.png");
		System.out.println(test2);
		test2.setSize(200);
		System.out.println(test2);

		Ordner c = new Ordner("c:");

		c.speichern(test1);
		c.speichern(test2);

		System.out.println(c);
		Ordner fotos = new Ordner("Fotos");
		fotos.speichern(new Datei("Passbild.png", 300));

		c.speichern(fotos);

		System.out.println(c);
		
		System.out.println(c.dateiGröße());
	}
}
