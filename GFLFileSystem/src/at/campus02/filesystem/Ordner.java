package at.campus02.filesystem;

import java.util.ArrayList;

public class Ordner {

	private String name;
	private ArrayList<Datei> meineDateien = new ArrayList<Datei>();
	private ArrayList<Ordner> meineOrdner = new ArrayList<Ordner>();

	public Ordner(String name) {
		this.name = name;
	}

	public void umbenennen(String neuerName) {
		this.name = neuerName;
	}

	public void speichern(Datei neueDatei) {
		meineDateien.add(neueDatei);
	}

	public void speichern(Ordner neuerOrdner) {
		meineOrdner.add(neuerOrdner);
	}

	public int anzahlDerEinträge() {
		int sum = meineDateien.size() + meineOrdner.size();

		for (Ordner einOrdner : meineOrdner) {
			sum = sum + einOrdner.anzahlDerEinträge();
		}

		return sum;
	}

	public int dateiGröße() {
		int summeGröße = 0;
		for (Datei datei : meineDateien) {
			summeGröße = summeGröße + datei.getSize();
		}
		
		for (Ordner einOrdner : meineOrdner) {
			summeGröße = summeGröße + einOrdner.dateiGröße();
		}
		
		return summeGröße;
	}

	public String toString() {
		return String.format("%s %d", name, this.anzahlDerEinträge());
	}
}
