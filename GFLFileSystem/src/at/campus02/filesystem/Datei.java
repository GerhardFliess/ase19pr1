package at.campus02.filesystem;

public class Datei {
	private String name;
	private int size;

	public Datei(String name) {
		this.name = name;
	}

	public Datei(String name, int size) {
		this.name = name;
		this.size = size;
	}

	public void setSize(int newSize) {
		this.size = newSize;
	}
	
	public int getSize() {
		return size;
	}

	public void umbenennen(String neuerName) {
		this.name = neuerName;
	}

	public String toString() {
		return String.format("%s %d", name, size);
	}

}
