package edu.campus02.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IntListDemo {

	public static void main(String[] args) {
		ArrayList<Integer> listeA = new ArrayList<Integer>();

		Collections.addAll(listeA, 2, 5, 7, 1, 2, 9, 5, 4, 2, 9);

		System.out.println(listeA);

		for (Integer wertAusListe : listeA) {
			System.out.println(wertAusListe);
		}

		int summe = sum(listeA);
		System.out.println(summe);

		listeA.add(0, 15); // einf�gen mit positionsangebe mit 0 anfangen zu
							// z�hlen
		System.out.println(listeA);

		System.out.printf("%d, %d \n", listeA.get(2), listeA.get(5)); // Element
																		// an
																		// einer
																		// bestimmten
																		// stelle
																		// zur�ck
																		// geben

		Collections.addAll(listeA, 3, 4, 5, 6);
		System.out.println(listeA);

		Collections.sort(listeA); //sortieren
		System.out.println(listeA);

		Collections.shuffle(listeA); //mischen
		System.out.println(listeA);
		

	}

	private static int sum(ArrayList<Integer> listeA) {
		int sum = 0;
		for (Integer wertAusListe : listeA) {
			sum = sum + wertAusListe;
		}

		return sum;
	}

	// Schreiben sie eine Methode die die Werte in der Liste summiert

}
