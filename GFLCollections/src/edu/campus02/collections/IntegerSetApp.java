package edu.campus02.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

public class IntegerSetApp {

	public static void main(String[] args) {
		HashSet<Integer> a = new HashSet<Integer>();

		a.add(2);
		a.add(1);
		Collections.addAll(a, 3, 5, 6, 7, 8, 9); // mehrere Werte auf
													// einmal einf�gen
		System.out.println(a.size());
		System.out.println(sum(a));

		HashSet<Integer> b = new HashSet<Integer>();
		Collections.addAll(b, 2, 3, 4);

		HashSet<Integer> c = combine(a, b);
		System.out.println(c);

		Integer[] test = new Integer[]{1, 2};
		Collections.addAll(a, test);
		sum(a);

		HashSet<Integer> d = schnittmenge(a, b);
	
		System.out.println(d);
	}

	private static HashSet<Integer> schnittmenge(HashSet<Integer> a,
			HashSet<Integer> b) {
		HashSet<Integer> d = new HashSet<Integer>();
		for (Integer wert : a) {
			if (b.contains(wert))
				d.add(wert);
		}
		return d;
	}

	private static HashSet<Integer> combine(HashSet<Integer> a,
			HashSet<Integer> b) {
		HashSet<Integer> c = new HashSet<Integer>();
		c.addAll(a);
		c.addAll(b);
		return c;
	}

	public static int sum(HashSet<Integer> set) {
		int sum = 0;

		for (Integer intValue : set) {
			sum = sum + intValue;
		}

		return sum;
	}

}
