package edu.campus02.collections;

import java.util.HashSet;

public class SetDemo {

	public static void main(String[] args) {

		HashSet<String> set = new HashSet<String>();

		set.add("spielt");
		System.out.println(set.add("ball"));
		System.out.println(set.add("ball"));
		set.add("mimi");

		System.out.println(set);
		System.out.println(set.size());

		for (String wort : set) {
			System.out.println(wort);
		}

		System.out.println(set.remove("mimi"));

		System.out.println(set);
		System.out.println(set.size());

		String[] stringArray = set.toArray(new String[set.size()]);
		
		
	}

}
