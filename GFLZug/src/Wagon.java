
public class Wagon {

	int wert;
	Wagon nachbar;
	
	public Wagon(int zahl)
	{
		wert = zahl;
	}
	
	public void zusammenhängen(Wagon neuerNachbar)
	{
		nachbar = neuerNachbar;
	}
	
	public Wagon gibMirDeinenNachbarn()
	{
		return nachbar;
	}
	
}
