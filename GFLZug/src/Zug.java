
public class Zug {

	Wagon erster;
	Wagon letzter;

	public void zahlAnhängen(int zahl) {

		Wagon neuerWagon = new Wagon(zahl);

		if (erster == null) // wir haben noch keinen Zahl eingefügt
		{
			erster = neuerWagon;
			letzter = neuerWagon;
		} else {
			letzter.zusammenhängen(neuerWagon); // Zusammenhängen
			letzter = neuerWagon;
		}
	}

	public void print() {
		// alle wagpne besuchen
		Wagon aktuellerWagon = erster;

		while (aktuellerWagon != null) {

			System.out.print(" " + aktuellerWagon.wert); // wert ausgeben

			aktuellerWagon = aktuellerWagon.gibMirDeinenNachbarn();
		}
	}

	public int sum() {
		int result = 0;
		Wagon aktuellerWagon = erster;

		while (aktuellerWagon != null) {

			result = result + erster.wert;
			aktuellerWagon = aktuellerWagon.gibMirDeinenNachbarn(); // zum
																	// nächsten
																	// Wagon
												// gehen
		}

		return result;
	}

	public int wertAnderStelle(int index) {
		Wagon aktuellerWagon = erster;

		for (int anzahl = 0; anzahl < index; anzahl++) {
			if (aktuellerWagon != null) {
				aktuellerWagon = aktuellerWagon.gibMirDeinenNachbarn();
			}
		}

		if(aktuellerWagon==null)
		{
			System.out.println("zu kurz");
			return -1;
		}
		return aktuellerWagon.wert;

	}

}
