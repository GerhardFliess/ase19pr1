
public class ZugApp {

	public static void main(String[] args) {
		Zug meinZahlenZug = new Zug();

		System.out.println(meinZahlenZug.wertAnderStelle(4));
		
		meinZahlenZug.zahlAnhängen(3);
		meinZahlenZug.zahlAnhängen(2);
		meinZahlenZug.zahlAnhängen(5);
		meinZahlenZug.zahlAnhängen(1);

		meinZahlenZug.print();
		System.out.println();
		System.out.println(meinZahlenZug.wertAnderStelle(0));
		System.out.println(meinZahlenZug.wertAnderStelle(1));
		System.out.println(meinZahlenZug.wertAnderStelle(2));
		System.out.println(meinZahlenZug.wertAnderStelle(3));
		
		System.out.println(meinZahlenZug.wertAnderStelle(4));
		System.out.println(meinZahlenZug.wertAnderStelle(10));
	}

}
