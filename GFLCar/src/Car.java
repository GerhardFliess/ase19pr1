
public class Car {
	String farbe;
	int nummer;
	double geschwindigkeit;
	double tankstand;
	boolean gestartet;

	public Car(String konkreteFarbe, int konkreteNummer) // Konstruktor
	{
		farbe = konkreteFarbe;
		nummer = konkreteNummer;
	}

	public void betanken(double liter) {

		tankstand = tankstand + liter;
	}

	public boolean starten() {
		
		if (tankstand > 0) {
			gestartet = true;
			return true;
		}

		return false;
	}
	
	public double gasGeben(double gaspedal)
	{
		if(!gestartet)
			return 0;
		if(tankstand==0)
			return 0;
		
		geschwindigkeit = geschwindigkeit+gaspedal;
		
		return geschwindigkeit;
	}

}
