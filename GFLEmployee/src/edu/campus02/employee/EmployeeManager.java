package edu.campus02.employee;

import java.util.ArrayList;

public class EmployeeManager {

	ArrayList<Employee> employees = new ArrayList<Employee>();

	public void add(Employee newEmployee) {
		employees.add(newEmployee);
	}

	public Employee findByEmpNumber(int number) {

		for (Employee employee : employees) {
			if (employee.number == number)
				return employee;
		}

		return null;
	}

	public Employee findByMaxSalery() {

		Employee maxSalery = employees.get(0); // ersten aus der liste merken

		for (Employee employee : employees) { // mit allen vergleichen
			if (maxSalery.salery < employee.salery) {
				maxSalery = employee;
			}
		}

		return maxSalery;
	}

	public ArrayList<Employee> findByDep(String dep) {
		ArrayList<Employee> result = new ArrayList<Employee>();
		for (Employee employee : employees) {
			if (employee.department == dep) {
				result.add(employee);
			}
		}

		return result;
	}

}
