package edu.campus02.employee;

public class Employee {
	int number;
	String name;
	String department;
	double salery;


	public Employee(int number, String name, String department, double salery) {	
		this.number = number;
		this.name = name;
		this.department = department;
		this.salery = salery;
	}



	public String toString() {
		return String.format("(%d, %s, %s, %.2f)", number, name, department,
				salery);
	}

}
