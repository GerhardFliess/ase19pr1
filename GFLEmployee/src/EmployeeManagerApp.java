import edu.campus02.employee.Employee;
import edu.campus02.employee.EmployeeManager;

public class EmployeeManagerApp {

	public static void main(String[] args) {

		EmployeeManager hrAbteilung = new EmployeeManager();

		hrAbteilung.add(new Employee(1, "Karl", "IT", 1800));
		hrAbteilung.add(new Employee(2, "Moni", "IT", 1900));
		hrAbteilung.add(new Employee(3, "Hans", "IT", 1500));
		hrAbteilung.add(new Employee(4, "Sepp", "Support", 1500));
		
		System.out.println(hrAbteilung.findByEmpNumber(2));
		
		System.out.println(hrAbteilung.findByEmpNumber(20));
		
		System.out.println(hrAbteilung.findByMaxSalery());
		
		System.out.println(hrAbteilung.findByDep("IT"));
		System.out.println(hrAbteilung.findByDep("Support"));
		System.out.println(hrAbteilung.findByDep("Marketing"));
	
	}

}
