import java.util.ArrayList;

import edu.campus02.employee.Employee;

public class EmployeeDemoApp {
	public static void main(String[] args) {
		ArrayList<Employee> employees = new ArrayList<Employee>();
		
		employees.add(new Employee(1, "Mimi", "devel", 2500));
		employees.add(new Employee(2, "Karl", "devel", 2500));

		Employee hans = new Employee(3, "Hans", "support", 2000);
		employees.add(hans);
		
		System.out.println(employees);
		
	}
}
